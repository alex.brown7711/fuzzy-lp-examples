# System Modules
import numpy as np

# Developed Modules
from .triangular_fuzzy import triangular_fuzzy
from .trapezoidal_fuzzy import trapezoidal_fuzzy

##===============================================================================
#
class FZN:
	##=======================================================================
	# PUBLIC

	##-----------------------------------------------------------------------
	# Input:
	#	a    : lower bound
	#	b    : if triangular; average value. if trapezoidal; lower average
	#	c    : if triangular; upper bound. if trapezoidal; upper average
	#	d    : if trapezoidal; upper bound
	#	type : trapezoidal, triangular
	#
	# Output:
	#	NONE
	#
	def __init__(self, a, b, c, d=0, type="triangular"):
		self.type=type

		if type == "trapezoidal":
			self.m_fuzzy = trapezoidal_fuzzy(a, b, c, d)
			self.a       = a
			self.b       = b
			self.c       = c
			self.d       = d
		elif type == "triangular":
			self.m_fuzzy = triangular_fuzzy(a, b, c)
			self.a       = a
			self.b       = b
			self.c       = c

		return
	
	##-----------------------------------------------------------------------
	# Input:
	#			NONE
	#
	# Output:
	#			Conversersion of fuzzy number to an array
	#
	def toArray(self):
		return np.array(self.m_fuzzy.toArray())
	
	##-----------------------------------------------------------------------
	# Input:
	#	other : Other fuzzy number
	#
	# Output:
	#	Addition of two fuzzy numbers
	#
	def __add__(self, other):
			return self.m_fuzzy + other

	##-----------------------------------------------------------------------
	# Input:
	#	other : Other fuzzy number
	#
	# Output:
	#	Subtraction of two fuzzy numbers
	#
	def __sub__(self, other):
			return self.m_fuzzy - other

	##-----------------------------------------------------------------------
	# Input:
	#	other : Other fuzzy number
	#
	# Output:
	#	Multiplication of two fuzzy numbers
	#
	def __mul__(self, other):
			return self.m_fuzzy * other

	##=======================================================================
	# PRIVATE
