# System Modules

# Developed Modules

##===============================================================================
#
class triangular_fuzzy:
	##=======================================================================
	# PUBLIC

	##-----------------------------------------------------------------------
	# Input:
	#	a: lower bound
	#	b: average
	#	c: upper bound
	#
	# Output:
	#	NONE
	#
	def __init__(self, a, b, c):
		# Ensure a valid input
		#  assert a < b and b < c, "INIT ERROR: invalid fuzzy number"

		# Update member variables
		self.a = a
		self.b = b
		self.c = c
		return
	
	##-----------------------------------------------------------------------
	# Input:
	#			NONE
	#
	# Output:
	#			Conversersion of triangular fuzzy number to an array
	#
	def toArray(self):
		return [self.a, self.b, self.c]

	##-----------------------------------------------------------------------
	# Input:
	#	other : Other fuzzy number
	#
	# Output:
	#	Addition of two fuzzy numbers
	#
	def __add__(self, other):
		# Local Variables
		## Fuzzy number 1
		a1 = self.a
		b1 = self.b
		c1 = self.c

		## Fuzzy number 2
		a2 = other.a
		b2 = other.b
		c2 = other.c

		return triangular_fuzzy(a1+a2, b1+b2, c1+c2)

	##-----------------------------------------------------------------------
	# Input:
	#	other : Other fuzzy number
	#
	# Output:
	#	Subtraction of two fuzzy numbers
	#
	def __sub__(self, other):
		# Local Variables
		## Fuzzy number 1
		a1 = self.a
		b1 = self.b
		c1 = self.c

		## Fuzzy number 2
		a2 = other.a
		b2 = other.b
		c2 = other.c

		return triangular_fuzzy(a1-c2, b1-b2, c1-a2)

	##-----------------------------------------------------------------------
	# Input:
	#	other : Other fuzzy number
	#
	# Output:
	#	Multiplication of two fuzzy numbers
	#
	def __mul__(self, other):
		# Local Variables
		## Fuzzy number 1
		a1 = self.a
		b1 = self.b
		c1 = self.c

		## Fuzzy number 2
		a2 = other.a
		b2 = other.b
		c2 = other.c

		return triangular_fuzzy(a1*a2, b1*b2, c1*c2)

	##=======================================================================
	# PRIVATE
