#!/usr/bin/python

# Standard Lib
import gurobipy as gp
import matplotlib as mp
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import numpy as np
import random
import sys

from gurobipy import GRB

# Include In Path
# sys.path.append("")

##===============================================================================
#
def generateConstList(value, size):
    return np.array([value]*size, dtype=int)

##===============================================================================
#
def generateRandomList(bounds, size):
    array = []

    for i in range(size):
        array.append(random.randint(bounds[0], bounds[1]))

    return np.array(array, dtype=int)

##===============================================================================
#
def plotResults(N, c, w, b, x):
    fig, axs = plt.subplots(1)
    x_axis   = np.arange(1,N+1)
    x        = np.array(x, dtype=int)

    axs.set_title("Crisp")
    axs.bar(x_axis, x)
    plt.show()
    return

##===============================================================================
# This model solves the following fuzzy linear problem
#   max sum c_j x_j
#
#   subject to
#
#   sum a_i x_i <= b
#   x is an element of {0,1}
#
def main():
    # Initialization Parameters
    ## Load previous generated list
    load_previous = True

    ## Number of items to solve over
    N = 20

    ## Bounds where cost may vary
    cost_bound = [1, 100]

    ## Bounds where weight may vary
    weight_bound = [1, 100]

    ## Max amount of item
    max_bound = [1, 5]

    ## Create a new model
    m = gp.Model("Fuzzy")

    # Set timeout for model
    #  m.setParam('TimeLimit', 1*60)

    # Given Parameters
    if load_previous:
        c = np.load("cost.npy")
        print("Loaded c: ", c)

        w = np.load("weight.npy")
        print("Loaded w: ", w)

        a = np.load("amount.npy")
        print("Loaded a: ", a)

        b = np.load("carry.npy")
        print("Loaded b: ", b)

    else:
        ## Max Capacity
        b = generateRandomList([50, 500], 1)[0]
        np.save("carry.npy", b)

        c = generateRandomList(cost_bound, N)
        np.save("cost.npy", c)

        w = generateRandomList(weight_bound, N)
        np.save("weight.npy", w)

        a = generateRandomList(max_bound, N)
        np.save("amount.npy", a)

    # Decision Variables
    x = m.addMVar(shape=N, vtype=GRB.INTEGER, name="x")

    # Objective funcvtion
    m.setObjective(sum(c[i]*x[i] for i in range(N)), GRB.MAXIMIZE)

    # Constraints
    m.addConstr((sum(w[i]*x[i] for i in range(N)) <= b), "w")

    for i in range(N):
        m.addConstr(x[i] <= a[i], "{i}")

    # Optimize
    m.optimize()

    # Display Results
    print("Problem Setup")
    print("a:\n", a)
    print("c:\n", c)
    print("w:\n", w)
    print("b:\n", b)

    print("Solution")
    print('x:\n', x.X)
    print('Obj: %g' % m.objVal)

    plotResults(N, c, w, b, x.X)

    return

##===============================================================================
#
if __name__ == "__main__":
    main()
