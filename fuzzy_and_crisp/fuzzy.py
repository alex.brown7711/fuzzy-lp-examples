#!/usr/bin/python

# Standard Lib
import gurobipy as gp
import matplotlib as mp
import matplotlib.pyplot as plt
import numpy as np
import random
import sys

from gurobipy import GRB

##===============================================================================
# CONSTANTS

# Initialization Parameters
## Load previous generated list
LOAD_PREVIOUS = False

## Number of items to solve over
N             = 20

## Bounds where cost may vary
COST_BOUND    = [[1, 2], [5, 8], [10, 15]]

## Bounds where weight may vary
WEIGHT_BOUND  = [[1, 2], [3, 4], [5, 6]]

## Max amount of item
MAX_BOUND     = [[0, 1], [6, 9], [10, 11]]

## Max Capacity
CARRY_BOUND   = [[20, 30], [50, 80], [90, 120]]

FUZZY_SIZE    = 3

##===============================================================================
#
def generateConstList(value, size):
    return np.array([value]*size, dtype=int)

##===============================================================================
#
def generateRandomList(bounds, size):
    array = []

    for i in range(size):
        array.append(random.randint(bounds[0], bounds[1]))

    return np.array(array, dtype=int)

##===============================================================================
# Input:
#   Bounds: Upper and lower allowable value for each part of the fuzzy number
#           [[lower/lower, lower/upper],
#            [average/lower, average/upper],
#            [upper/lower, upper/upper]]
#   Size: Amount of numbers in the list
#
def generateFuzzyRandomList(bounds, size):
    lower   = []
    average = []
    upper   = []

    for i in range(size):
        lower.append(random.randint(bounds[0][0]   , bounds[0][1]))
        average.append(random.randint(bounds[1][0] , bounds[1][1]))
        upper.append(random.randint(bounds[2][0]   , bounds[2][1]))

    return TriangleFuzzyNum(lower, average, upper, int)

##===============================================================================
#
def plotResults(N, c, w, b, x):
    fig, axs = plt.subplots(1)
    x_axis   = np.arange(1,N+1)
    x_low    = np.array(x[0], dtype=int)
    x_ave    = np.array(x[1], dtype=int)
    x_high   = np.array(x[2], dtype=int)
    error    = np.array([x_ave-x_low, x_high-x_ave])

    axs.set_title("Fuzzy")
    axs.bar(x_axis, x_ave, yerr=error)

    plt.show()
    return

##===============================================================================
# Return a (3,N) array of fuzzy numbers
#
def TriangleFuzzyArray(lower, average, upper, my_type):
    return np.array([[lower], [average], [upper]], dtype=my_type)

##===============================================================================
# Return a fuzzy number
#
def TriangleFuzzyNum(lower, average, upper, my_type):
    return np.array([lower, average, upper], dtype=my_type)

##===============================================================================
# This model solves the following fuzzy linear problem
#   max sum c_j x_j
#
#   subject to
#
#   sum a_i x_i <= b
#   x is an element of {0,1}
#
# Where c, x, a, and b are all fuzzy numbers
#
def main():
    ## Create a new model
    m = gp.Model("Fuzzy")

    # Set timeout for model
    #  m.setParam('TimeLimit', 1*60)

    # Given Parameters
    if LOAD_PREVIOUS:
        c = np.load("fuzzy_cost.npy")
        print("Loaded c: ", c)

        w = np.load("fuzzy_weight.npy")
        print("Loaded w: ", w)

        a = np.load("fuzzy_amount.npy")
        print("Loaded a: ", a)

    else:
        c = generateFuzzyRandomList(COST_BOUND, N)
        np.save("fuzzy_cost.npy", c)
        np.save("cost.npy", c[1])

        w = generateFuzzyRandomList(WEIGHT_BOUND, N)
        np.save("fuzzy_weight.npy", w)
        np.save("weight.npy", w[1])

        a = generateFuzzyRandomList(MAX_BOUND, N)
        np.save("fuzzy_amount.npy", a)
        np.save("amount.npy", a[1])

    b = generateFuzzyRandomList(CARRY_BOUND, 1)
    np.save("fuzzy_carry.npy", b)
    np.save("carry.npy", b[1])

    # Decision Variables
    x = m.addMVar(shape=(FUZZY_SIZE,N), vtype=GRB.INTEGER, name="x")

    # Objective funcvtion
    m.setObjective((sum(c[j][i]*x[j][i] for i in range(N)               \
                                        for j in range(FUZZY_SIZE))/3), \
                                        GRB.MAXIMIZE)

    # Constraints
    for k in range(FUZZY_SIZE):
        m.addConstr((sum(w[k][i]*x[k][i] for i in range(N)) <= b[k][0]), "w{k}")

        for i in range(N):
            m.addConstr(x[k][i] <= a[k][i], "{i}")

            #  m.addConstr(x[0][i] <= x[1][i], "fuzzy{i}")
            #  m.addConstr(x[1][i] <= x[2][i], "fuzzy{i+1}")

    # Optimize
    m.optimize()

    # Display Results
    print("Problem Setup")
    print("a:\n", a)
    print("c:\n", c)
    print("w:\n", w)
    print("b:\n", b)

    print("Solution")
    print('x:\n', x.X)
    print('Obj: %g' % m.objVal)

    plotResults(N, c, w, b, x.X)

    return

##===============================================================================
#
if __name__ == "__main__":
    main()
