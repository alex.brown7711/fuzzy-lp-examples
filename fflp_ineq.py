#!/usr/bin/python

# Standard Lib
import gurobipy as gp

from gurobipy import GRB

# Developed Modules
from fuzzy_lib.fuzzy_number import FZN as fzn

##===============================================================================
# The following example is found in @nasseri_2013 in notes.
#
# Reference:
#   SH Nasseri, E Behmanesh, F Taleshian, M Abdolalipoor, and NEZHAD NA TAGHI.
#   Fully fuzzy linear programming with inequality constraints. 2013.
#
def main():
	# initialize problem
	## Objective fuzzy numbers
	o1 = fzn(1,2,3)
	o2 = fzn(2,3,4)

	## Constraint fuzzy numbers
	c11 = fzn(0,1,2)
	c12 = fzn(1,2,3)
	b1  = fzn(1,10,27)

	c21 = fzn(1,2,3)
	c22 = fzn(0,1,2)
	b2  = fzn(2,11,28)

	# Create and solve model
	## Create model
	m = gp.Model()

	## Create variables
	x1_constr = m.addMVar(shape=3, vtype=GRB.CONTINUOUS, name="x1")
	x2_constr = m.addMVar(shape=3, vtype=GRB.CONTINUOUS, name="x2")

	## Convert variables to fuzzy number
	x1        = fzn(x1_constr[0], x1_constr[1], x1_constr[2])
	x2        = fzn(x2_constr[0], x2_constr[1], x2_constr[2])

	#  print((c11*x1).toArray())
	#  print((c11*x1 + c12*x2).toArray())

	## Create constraints
	for i in range(3):
		m.addConstr( (c11*x1 + c12*x2).toArray()[i] <= b1.toArray()[i] , name="c1+{0}".format(i))
		m.addConstr( (c21*x1 + c22*x2).toArray()[i] <= b2.toArray()[i] , name="c2+{0}".format(i))

	## Create objective
	m.setObjective(sum((o1*x1 + o2*x2).toArray())/4, GRB.MAXIMIZE)

	# Uncomment to print model to disk
	m.write("model.lp")

	## Optimize
	m.optimize()

	## Print results
	print(x1_constr.X)
	print(x2_constr.X)
	print('Obj: %g' % m.objVal)

	return

##===============================================================================
#
if __name__ == "__main__":
	main()
